variable "prefix" {
  default = "salsa"
}

variable "project" {
  default = "salsa_app"
}

variable "bastion_key_name" {
  default = "salsa-app-bastion"
}


variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "632365357478.dkr.ecr.us-east-1.amazonaws.com/salsa-app:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "632365357478.dkr.ecr.us-east-1.amazonaws.com/salsa_app_proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
