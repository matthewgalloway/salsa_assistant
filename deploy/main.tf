terraform {
  backend "s3" {
    bucket         = "salsa-app-tfstate"
    key            = "salsa-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "salsa-app-tf-state-lock"
  }

}

provider "aws" {
  region  = "us-east-1"
  version = "~> 4.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    ManagedBy   = "Terraform"
  }
}

locals {
}

data "aws_region" "current" {}